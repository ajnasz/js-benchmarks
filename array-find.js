'use strict';
const Benchmark = require('benchmark');
const _ = require('lodash');

const suite = new Benchmark.Suite();

const array = _.shuffle([
	{a: 1},
	{a: 2},
	{a: 3},
	{a: 4},
	{a: 5},
	{a: 6},
	{a: 7},
	{a: 8},
	{a: 9},
	{a: 10},
	{a: 11},
	{a: 12},
	{a: 13},
	{a: 14},
	{a: 15},
	{a: 16},
	{a: 17},
	{a: 18},
	{a: 19},
	{a: 20},

]);

const cb = x => x.a === 20;

function myFind(array, cb) {
	for (let i = 0, al = array.length; i < al; i++) {
		if (cb(array[i])) {
			return array[i];
		}
	}

	return null;
}

// add tests
suite
	.add('native find', () => array.find(cb))
	.add('lodash find', () => _.find(array, cb))
	.add('my find', () => myFind(array, cb))
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
// run async
	.run({ 'async': true });

// vi:sw=2:ts=2:noexpandtab
