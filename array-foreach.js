const _ = require('lodash');
const Benchmark = require('benchmark');

const array = [
	{a: 1},
	{a: 2},
	{a: 3},
	{a: 4},
	{a: 5},
	{a: 6},
	{a: 7},
	{a: 8},
	{a: 9},
	{a: 10},
	{a: 11},
	{a: 12},
	{a: 13},
	{a: 14},
	{a: 15},
	{a: 16},
	{a: 17},
	{a: 18},
	{a: 19},
	{a: 20},

];

const suite = new Benchmark.Suite();

function cb(item) {
	item.b = item.a * 2;
}

suite
	.add('arrayForeach', () => {
		array.forEach(cb);
	})
	.add('lodash.foreach', () => {
		_.forEach(array, cb);
	})
	.add('for of', () => {
		for (const i of array) {
			i.b = i.a * 2;
		}
	})
// add listeners
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
// run async
	.run({ 'async': true });
