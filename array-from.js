'use strict';
const Benchmark = require('benchmark');
const _ = require('lodash');

const suite = new Benchmark.Suite();

function* generator() {
	let i = 0;

	while (i < 100) {
		yield i++;
	}
}

suite
	.add('native Array.from', () => Array.from(generator()))
	.add('Array.prototype.slice.call', () => Array.prototype.slice.call(generator()))
	.add('lodash _.toArray', () => _.toArray(generator()))
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
// run async
	.run({ 'async': true });
