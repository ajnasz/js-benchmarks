'use strict';
const Benchmark = require('benchmark');
const _ = require('lodash');

const suite = new Benchmark.Suite();

const array = [
	{a: 1},
	{a: 2},
	{a: 3},
	{a: 4},
	{a: 5},
	{a: 6},
	{a: 7},
	{a: 8},
	{a: 9},
	{a: 10},
	{a: 11},
	{a: 12},
	{a: 13},
	{a: 14},
	{a: 15},
	{a: 16},
	{a: 17},
	{a: 18},
	{a: 19},
	{a: 20},

];

const cb = (x) => {
	if (x.a % 2) {
		Object.assign({}, x, {b: x.a * x.a});
	}

	return x;
};

// const cbmut = (x) => {
// 	if (x.a % 2) {
// 		Object.assign(x, {b: x.a * x.a});
// 	}
// 	return x;
// };

function mapArray(array, cb) {
	return array.map(cb);
}

function lodashMapArray(array, cb) {
	return _.map(array, cb);
}

function lodashReduceArray(array, cb) {
	return _.reduce(array, (output, item, index) => {
		output[index] = cb(item);
		return output;
	}, []);
}

function reduceArray(array, cb) {
	return array.reduce((output, item, index) => {
		output[index] = cb(item);
		return output;
	}, []);
}

function reduceArrayPush(array, cb) {
	return array.reduce((output, item) => {
		output.push(cb(item));
		return output;
	}, []);
}

function forEachArrayPush(array, cb) {
	const output = [];
	array.forEach((item) => {
		output.push(cb(item));
	});

	return output;
}


function lodashForEachArrayPush(array, cb) {
	const output = [];
	_.forEach(array, (item) => {
		output.push(cb(item));
	});

	return output;
}

function lodashReduceArrayPush(array, cb) {
	return _.reduce(array, (output, item) => {
		output.push(cb(item));
		return output;
	}, []);
}

function reduceArrayConcat(array, cb) {
	return array.reduce((output, item) => output.concat(cb(item)), []);
}

function lodashReduceArrayConcat(array, cb) {
	return _.reduce(array, (output, item) => output.concat(cb(item)), []);
}

function reduceArraySetIndex(array, cb) {
	return array.reduce((output, item, index) => {
		output[index] = cb(item);
		return output;
	}, new Array(array.length));
}

function forEachArraySetIndex(array, cb) {
	const output = new Array(array.length);

	array.forEach((item, index) => {
		output[index] = cb(item);
		return output;
	});

	return output;
}

function lodashReduceArraySetIndex(array, cb) {
	return _.reduce(array, (output, item, index) => {
		output[index] = cb(item);
		return output;
	}, new Array(array.length));
}

function flatmapArray(array, cb) {
	return array.flatMap(elem => cb(elem));
}

function lodashFlatmapArray(array, cb) {
	return _.flatMap(array, elem => cb(elem));
}

// function forEach(array, cb) {
// 	array.forEach(cb);
// 	return array;
// }

// add tests
suite
	.add('mapArray', () => {
		mapArray(array, cb);
	})
	.add('lodashMapArray', () => {
		lodashMapArray(array, cb);
	})
	.add('flatMapArray', () => {
		mapArray(array, cb);
	})
	.add('lodashFlatMapArray', () => {
		mapArray(array, cb);
	})
	// .add('forEach', () => {
	// 	forEach(array, cb);
	// })
	// .add('forEachMut', () => {
	// 	console.log('array1', array)
	// 	console.log('array2', forEach(array, cbmut));
	// })
	.add('reduceArray', () => {
		reduceArray(array, cb);
	})
	.add('lodashReduceArray', () => {
		lodashReduceArray(array, cb);
	})
	.add('forEachArrayPush', () => {
		forEachArrayPush(array, cb);
	})
	.add('lodashForEachArrayPush', () => {
		lodashForEachArrayPush(array, cb);
	})
	.add('reduceArrayPush', () => {
		reduceArrayPush(array, cb);
	})
	.add('lodashReduceArrayPush', () => {
		lodashReduceArrayPush(array, cb);
	})
	.add('reduceArrayConcat', () => {
		reduceArrayConcat(array, cb);
	})
	.add('lodashReduceArrayConcat', () => {
		lodashReduceArrayConcat(array, cb);
	})
	.add('reduceArraySetIndex', () => {
		reduceArraySetIndex(array, cb);
	})
	.add('lodashReduceArraySetIndex', () => {
		lodashReduceArraySetIndex(array, cb);
	})
	.add('forEachArraySetIndex', () => {
		forEachArraySetIndex(array, cb);
	})
// add listeners
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
// run async
	.run({ 'async': true });

// vi:sw=2:ts=2:noexpandtab
