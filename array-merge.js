const Benchmark = require('benchmark');

const suite = new Benchmark.Suite();
const array = [
	{a: 1},
	{a: 2},
	{a: 3},
	{a: 4},
	{a: 5},
	{a: 6},
	{a: 7},
	{a: 8},
	{a: 9},
	{a: 10},
	{a: 11},
	{a: 12},
	{a: 13},
	{a: 14},
	{a: 15},
	{a: 16},
	{a: 17},
	{a: 18},
	{a: 19},
	{a: 20},
];
const array2 = [
	{a: 21},
	{a: 42},
	{a: 63},
	{a: 84},
	{a: 105},
	{a: 126},
	{a: 147},
	{a: 168},
	{a: 189},
	{a: 210},
	{a: 231},
	{a: 252},
	{a: 273},
	{a: 294},
	{a: 315},
	{a: 336},
	{a: 357},
	{a: 378},
	{a: 399},
	{a: 420},
];


suite
	.add('push spread', () => {
		const arr = [...array];
		arr.push(...array2);
	})
	.add('push apply', () => {
		const arr = [...array];
		arr.push.apply(arr, array2);
	})
// add listeners
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
// run async
	.run({ 'async': true });
