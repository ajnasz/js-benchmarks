'use strict';
const Benchmark = require('benchmark');
const _ = require('lodash');

const array = ('asdlfjasdlkfj lasdkfjdklsfjlasdjfl adksjfas ldfkjasdlkajsf' +
  'ladksfjklsfjasl kjasdfkljasfklf jasdflj23lkj3r8934qujifk3094q4ucmn2r30lakjfm').split('');

function getString(i) {
  return i;
}

const suite = new Benchmark.Suite();

suite
  .add('for plus concat', () => {
    let string = '';
    for (let i = 0, al = array.length; i < al; i++) {
      string += array[i];
    }

    return string;
  })
  .add('for plus concat func call', () => {
    let string = '';
    for (let i = 0, al = array.length; i < al; i++) {
      string += getString(array[i]);
    }

    return string;
  })
  .add('for plus template literal', () => {
    let string = '';
    for (let i = 0, al = array.length; i < al; i++) {
      string = `${string}${array[i]}`;
    }

    return string;
  })
  .add('array foreach', () => {
    let string = '';
    array.forEach(i => string += i);
	return string
  })
  .add('array reduce', () => {
    const string = array.reduce((o, i) => o + i, '');
    return string;
  })
  .add('lodash array reduce', () => {
    const string = _.reduce(array, (o, i) => o + i, '');
    return string;
  })
// add listeners
  .on('cycle', (event) => {
    console.log(String(event.target));
  })
  .on('complete', function() {
    console.log('Fastest is ' + this.filter('fastest').map('name'));
  })
  .on('error', function (err) {
	  console.error(err);
  })
// run async
  .run({ 'async': true });

// vi:sw=2:ts=2:expandtab
