'use strict';
const Benchmark = require('benchmark');
const _ = require('lodash');

const suite = new Benchmark.Suite();

const array = [
	{a: 1},
	{a: 2},
	{a: 3},
	{a: 4},
	{a: 5},
	{a: 6},
	{a: 7},
	{a: 8},
	{a: 9},
	{a: 10},
	{a: 11},
	{a: 12},
	{a: 13},
	{a: 14},
	{a: 15},
	{a: 16},
	{a: 17},
	{a: 18},
	{a: 19},
	{a: 20},

];

const cb = x => x.a === 20;

function nativeReduce(array, cb) {
	return array.reduce((output, item, index) => {
		if (output !== -1) return output;
		if (cb(item)) return index;

		return -1;
	}, -1);
}

function lodashReducev2(array, cb) {
	return _.reduce((output, item, index) => {
		if (output !== -1) return output;
		if (cb(item)) return index;

		return output;
	}, -1);
}

function lodashReduce(array, cb) {
	return _.reduce((output, item, index) => {
		if (output !== -1) return output;
		if (cb(item)) return index;

		return -1;
	}, -1);
}

function myFindIndex(array, cb) {
	for (let i = 0, al = array.length; i < al; i++) {
		if (cb(array[i])) {
			return i;
		}
	}

	return -1;
}

// add tests
suite
	.add('nativeReduce', () => nativeReduce(array, cb))
	.add('lodashReduce', () => lodashReduce(array, cb))
	.add('lodashReducev2', () => lodashReducev2(array, cb))
	.add('lodash findIndex', () => _.findIndex(array, cb))
	.add('native findIndex', () => array.findIndex(cb))
	.add('my find index', () => myFindIndex(array, cb))
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
// run async
	.run({ 'async': true });

// vi:sw=2:ts=2:noexpandtab
