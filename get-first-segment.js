'use strict';
const Benchmark = require('benchmark');

const str = 'asdlkfj_lksdjf_kalskd_fjfalskdfj_asldkjfa_sldfjaslkdfj_asldkfjas_dlfkjaslkfjaslkdfja_slkfjla_ksdjdflkasdjf_asdfklj_sdf';
const suite = new Benchmark.Suite();

suite
	.add('split and and get first element', () => str.split('_')[0])
	.add('find first index', () => {
		const idx = str.indexOf('_');

		if (idx === -1) {
			return str;
		}
		return str.slice(0, idx);
	})
// add listeners
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
	.on('error', (err) => {
		console.error(err);
	})
// run async
	.run({ 'async': true });
