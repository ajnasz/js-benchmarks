'use strict';
const Benchmark = require('benchmark');
const _ = require('lodash');

const suite = new Benchmark.Suite();

const events = _.shuffle([
	{a: 1},
	{a: 2},
	{a: 3},
	{a: 4},
	{a: 5},
	{a: 6},
	{a: 7},
	{a: 8},
	{a: 9},
	{a: 10},
	{a: 11},
	{a: 12},
	{a: 13},
	{a: 14},
	{a: 15},
	{a: 16},
	{a: 17},
	{a: 18},
	{a: 19},
	{a: 20},
]);

const cb = (state, i) => {
	if (i.a % 2) {
		return Object.assign({}, state, i);
	}

	return state;
};

const cbMut = (state, i) => {
	if (i.a % 2) {
		return Object.assign(state, i);
	}

	return state;
};

suite
	.add('reduce', () => events.reduce(cb, {}))
	.add('reduceMut', () => events.reduce(cbMut, {}))
	.add('foreachMut', () => {
		const state = {};

		events.forEach((e) => {
			cbMut(state, e);
		});

		return state;
	})
	.add('foreach no callback', () => {
		const state = {};

		events.forEach((e) => {
			state.a = e.a;
		});

		return state;
	})
	.add('lodash reduce', () => _.reduce(events, cb, {}))
	.add('lodash reduceMut', () => _.reduce(events, cbMut, {}))
	.add('lodash foreachMut', () => {
		const state = {};

		_.forEach(events, (e) => {
			cbMut(state, e);
		});

		return state;
	})
	.add('lodash foreach no callback', () => {
		const state = {};

		_.forEach(events, (e) => {
			state.a = e.a;
		});

		return state;
	})
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
	.run({ 'async': true });
