'use strict';
const Benchmark = require('benchmark');

const str = 'asdlkfj_lksdjf_kalskd_fjfalskdfj_asldkjfa_sldfjaslkdfj_asldkfjas_dlfkjaslkfjaslkdfja_slkfjla_ksdjdflkasdjf_asdfklj_sdf'
const suite = new Benchmark.Suite();

const regexp = /_/g;
suite
	.add('split and join', () => {
	  return str.split('_').join(' ');
	})
	.add('regexp replace', () => {
		return str.replace(regexp, ' ');
	})
// add listeners
	.on('cycle', (event) => {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
	.on('error', function (err) {
		console.error(err);
	})
// run async
	.run({ 'async': true });
